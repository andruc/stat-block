import actions from './actions'
import initPC from './defaultpc'

const BASE_ABILITIES = { str:10, dex:10, con:10, int:10, wis:10, cha:10 }

// saves some boilerplate for each of the ability scores
// an ability score's shortname is passed in, and a reducer is returned
function abilityActionGenerator (score) {
  return (state = {}, action) => {
    let intValue = parseInt(action.value, 10)

    return Object.assign({}, state, {
      abilities: Object.assign({}, (state.abilities || BASE_ABILITIES), {
        [score]: intValue
      }),
      modifiers: Object.assign({}, state.modifiers, {
        [score]: Math.floor((intValue - 10) / 2)
      })
    })
  }
}

const reducerHash = {
  [actions.CHANGE_NAME]: (state, action) => {
    return Object.assign({}, state, { name : action.value })
  },
  [actions.CHANGE_RACE]: (state, action) => {
    return Object.assign({}, state, { race : action.value })
  },
  [actions.CHANGE_CLASS]: (state, action) => {
    return Object.assign({}, state, { class : action.value })
  },
  [actions.CHANGE_ALIGNMENT]: (state, action) => {
    return Object.assign({}, state, { alignment : action.value })
  },

  // Ability Scores
  [actions.SET_STR]: abilityActionGenerator('str'),
  [actions.SET_DEX]: abilityActionGenerator('dex'),
  [actions.SET_CON]: abilityActionGenerator('con'),
  [actions.SET_INT]: abilityActionGenerator('int'),
  [actions.SET_WIS]: abilityActionGenerator('wis'),
  [actions.SET_CHA]: abilityActionGenerator('cha'),
}

export default function reducer (state = {}, action) {
  return action.type in actions ? reducerHash[action.type](state, action) : initPC
}
