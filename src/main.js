import actionFactory from './actionFactory'
import initPC from './defaultpc'

import { createStore } from 'redux'
import reducer from './reducers.js'

let store = createStore(reducer)

function renderModifier (mod) {
  return isFinite(mod) ? '('+ (mod >= 0 ? '+' : '') + mod  +')' : ''
}

function renderEntireBlock (character) {
  $('.statblock .name').text(character.name || initPC.name)
  $('.statblock .race').text(character.race || initPC.race)
  $('.statblock .class').text(character.class || initPC['class'])
  $('.statblock .alignment').text(character.alignment || initPC.alignment)

  ;['str', 'dex', 'con', 'int', 'wis', 'cha'].forEach((scoreName) => {
    $('.statblock .ability-score-'+ scoreName)
      .text(((character.abilities[scoreName] || '') +' '+
        renderModifier(character.modifiers[scoreName])))
  })
}

// log all updates made to the store
store.subscribe(() => { console.dir(store.getState()) })

store.subscribe(() => { renderEntireBlock(store.getState()) })

renderEntireBlock(initPC)

// bind datastore update to each input field change
$(document.body).on('input', 'input', (event) => {
  let field = $(event.currentTarget).attr('data-action')
  let value = event.currentTarget.value

  store.dispatch(actionFactory(field, value))
})
