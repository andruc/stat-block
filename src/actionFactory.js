import actions from './actions'

export default function (field, value) {
  switch (field) {
    case 'character-name':
      return { type: actions.CHANGE_NAME, value }
    case 'character-race':
      return { type: actions.CHANGE_RACE, value }
    case 'character-class':
      return { type: actions.CHANGE_CLASS, value }
    case 'character-alignment':
      return { type: actions.CHANGE_ALIGNMENT, value }

    case 'score_str':
      return { type: actions.SET_STR, value }
    case 'score_dex':
      return { type: actions.SET_DEX, value }
    case 'score_con':
      return { type: actions.SET_CON, value }
    case 'score_int':
      return { type: actions.SET_INT, value }
    case 'score_wis':
      return { type: actions.SET_WIS, value }
    case 'score_cha':
      return { type: actions.SET_CHA, value }

    default:
      return { type: 'NO_CHANGE' }
  }
}