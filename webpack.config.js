var path = require('path');

module.exports = {
    entry: './src/main.js',
    devtool: 'source-map',
    output: {
        path: __dirname +'/build',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { test: path.join(__dirname, 'src'),
              loader: 'babel-loader',
              query: { presets: ['es2015'] } }
        ]
    }
};
